<?php

namespace Drupal\ckeditor_layouts\Plugin\CKEditorPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "ckeditor_layouts" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_layouts",
 *   label = @Translation("Layouts"),
 * )
 */
class Layouts extends CKEditorPluginBase implements CKEditorPluginInterface, CKEditorPluginButtonsInterface, CKEditorPluginCssInterface, CKEditorPluginConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The module list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->layoutPluginManager = $container->get('plugin.manager.core.layout');
    $instance->libraryDiscovery = $container->get('library.discovery');
    $instance->renderer = $container->get('renderer');
    $instance->moduleList = $container->get('extension.list.module');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $settings = $editor->getSettings();

    $available_layouts = [];
    $layout_plugins = $this->layoutPluginManager->getSortedDefinitions();
    foreach ($layout_plugins as $plugin) {
      // The layout_builder_blank plugin is for internal use by Layout Builder
      // only.
      if ($plugin->id() === 'layout_builder_blank') {
        continue;
      }
      $available_layouts[$plugin->id()] = $plugin->getLabel();
    }

    $form['enabled_layouts'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled layouts'),
      '#description' => $this->t('Only the layouts that are selected here are available in the editor.'),
      '#multiple' => TRUE,
      '#options' => $available_layouts,
      '#default_value' => $settings['plugins']['ckeditor_layouts']['enabled_layouts'] ?? array_keys($available_layouts),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [];

    // Get the plugins for enabled layouts.
    $enabled_layouts = $editor->getSettings()['plugins']['ckeditor_layouts']['enabled_layouts'] ?? [];
    $layouts = array_filter($this->layoutPluginManager->getSortedDefinitions(), function ($layout) use ($enabled_layouts) {
      if (isset($enabled_layouts[$layout->id()]) && $enabled_layouts[$layout->id()]) {
        return TRUE;
      }

      return FALSE;
    });

    foreach ($layouts as $layout) {
      // Prepare the icon for rendering.
      $icon = $this->preprocessIcon($layout->getIcon());

      // Prepare the template for rendering. Rendering is based on a theme hook.
      // Rendering of regions in a layout might be conditional based on actual
      // content in the region. In the editor we always want all regions to be
      // rendered so that content can be added in any available region.
      // Therefore we put an empty markup element in each region.
      $template = ['#theme' => $layout->getThemeHook()];
      foreach ($layout->getRegionNames() as $region) {
        $template[$region] = ['#markup' => ''];
      }

      // Add the layout to the JavaScript settings.
      $config['ckeditorLayouts_layouts'][] = [
        'id' => $layout->id(),
        'label' => $layout->getLabel(),
        // Add the rendered icon and template. We filter out HTML comments
        // just in case Twig debug is enabled.
        'image' => preg_replace('/<!--(.|\s)*?-->/', '', $this->renderer->render($icon)),
        'template' => preg_replace('/<!--(.|\s)*?-->/', '', $this->renderer->render($template)),
      ];
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $plugin_path = $this->moduleList->getPath('ckeditor_layouts');
    return $plugin_path . '/js/plugins/layouts/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $plugin_path = $this->moduleList->getPath('ckeditor_layouts');
    $path = $plugin_path . '/js/plugins/layouts/icons/';
    return [
      'AddLayout' => [
        'label' => $this->t('Layouts'),
        'image' => $path . 'layouts.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor) {
    $css_files = [];

    // Editor CSS.
    $plugin_path = $this->moduleList->getPath('ckeditor_layouts');
    $css_files[] = $plugin_path . '/js/plugins/layouts/' . 'layouts.css';

    // Layouts CSS.
    foreach ($this->layoutPluginManager->getDefinitions() as $layout) {
      $layout_library = $layout->getLibrary();
      if ($layout_library) {
        [$module, $library_name] = explode('/', $layout_library);
        $library = $this->libraryDiscovery->getLibraryByName($module, $library_name);
        if ($library) {
          foreach ($library['css'] as $css) {
            $css_files[] = $css['data'];
          }
        }
      }
    }

    return $css_files;
  }

  /**
   * Prepares an icon for display in a dialog.
   *
   * @param array $icon
   *   The renderable array for the icon.
   *
   * @return array
   *   The updated renderable array for the icon.
   */
  protected function preprocessIcon(array $icon) {
    // The reset in core/assets/vendor/ckeditor/skins/moono-lisa/dialog.css
    // makes the icon invisible when it is an SVG. Therefore we put some inline
    // styling on the elements. We assume an SVG in the format returned by
    // \Drupal\Core\Layout\Icon\SvgIconBuilder.
    if ('html_tag' === $icon['#type'] && 'svg' === $icon['#tag']) {
      $style = '';
      if (isset($icon['#attributes']['height'])) {
        $style .= "height:" . $icon['#attributes']['height'] . 'px;';
      }
      if (isset($icon['#attributes']['width'])) {
        $style .= "width:" . $icon['#attributes']['width'] . 'px;';
      }
      $icon['#attributes']['style'] = $style;

      if (isset($icon['region'])) {
        foreach ($icon['region'] as $name => $region) {
          if (isset($region['rect']) && $region['rect']['#type'] === 'html_tag' && $region['rect']['#tag'] === 'rect') {
            $region_style = '';
            if (isset($region['rect']['#attributes']['height'])) {
              $region_style .= "height:" . $region['rect']['#attributes']['height'] . 'px;';
            }
            if (isset($region['rect']['#attributes']['width'])) {
              $region_style .= "width:" . $region['rect']['#attributes']['width'] . 'px;';
            }
            $icon['region'][$name]['rect']['#attributes']['style'] = $region_style;
          }
        }
      }
    }

    return $icon;
  }

}
