CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Using default layouts
 * Defining your own layouts
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

This module, used on top of CKEditor, allows the use of layouts defined via the core Layout API within WYSIWYG content.

 * For a full description of the module visit the [project page](https://www.drupal.org/project/ckeditor_layouts).

 * To submit bug reports and feature suggestions or to track changes see the [issue list](https://www.drupal.org/project/issues/ckeditor_layouts?categories=All).


REQUIREMENTS
------------

This module requires the following module:

 * CKEditor (included in Drupal core)
 * Layout Discovery (included in Drupal core)


INSTALLATION
------------

1. [Install the module](https://www.drupal.org/docs/extending-drupal/installing-modules) as any other contributed module
   for Drupal.

2. Add the Layouts plugin to the editor configuration of any text format
   (i.e. /admin/config/content/formats/manage/full_html) by dragging the button on the toolbar (the button is the one
   showed in js/plugins/layouts/images/layouts.png).

3. Optionally limit the allowed layouts on the Layouts plugin settings tab in the editor configuration.

The layouts available in the editor can be defined via the core Layout API. See the following sections.


USING DEFAULT LAYOUTS
------------

The Layout Discovery module in Drupal core will provide some default layouts.

* The text format you are using should allow div to be used within the textarea. In case you are using the
  "Limit allowed HTML tags" filter you should add <div class=""> in the allowed HTML tags list.
* Make sure the classes added by the layouts are also defined in the CSS of your front end theme.


DEFINING YOUR OWN LAYOUTS
------------

Custom layouts can be added via the core [Layout API](https://www.drupal.org/docs/drupal-apis/layout-api/how-to-register-layouts).

Be aware that you need to include CSS in your front end theme to actually turn the injected HTML into nice layouts!


TROUBLESHOOTING
---------------

 * If the button does not display on the toolbar check whether the button is present on the editor toolbar
   configuration for the respective text format.
 * In case your columns are not displaying check
   - That `<div class="">` or other markup you have used in the layouts are present in the allowed HTML tag list.
   - That you have defined CSS in your theme to actually turn the markup into layouts.


MAINTAINERS
-----------

Current maintainers:
 * Rico van de Vin (ricovandevin) - https://www.drupal.org/u/ricovandevin
 * Sean Blommaert (seanB) - https://www.drupal.org/u/seanb
