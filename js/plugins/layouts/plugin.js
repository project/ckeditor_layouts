/**
 * @file
 * Drupal Layouts plugin.
 */

(function (Drupal, CKEDITOR) {
  // Add our plugin to CKEditor.
  CKEDITOR.plugins.add('ckeditor_layouts', {
    icons: 'layouts',
    beforeInit: function (editor) {
      // Add a dialog for selection of the layout to be inserted.
      CKEDITOR.dialog.add('AddLayoutsDialog' + editor.name, function (editor) {
        let default_layout_id = null;

        // Build an options array with all layouts.
        let layouts = [];
        editor.config.ckeditorLayouts_layouts.forEach(function (layout, index) {
          // Mark the first item as the default.
          if (null === default_layout_id) {
            default_layout_id = layout.id;
          }
          layouts.push([Drupal.theme('ckEditorLayoutsLayoutLabel', layout.label, layout.image), layout.id]);
        });

        // Return tha definition of the dialog.
        return {
          title: Drupal.t('Insert a layout'),
          minWidth: 400,
          minHeight: 200,
          contents: [
            {
              id: 'layouts',
              label: 'Layouts',
              elements: [
                {
                  type: 'radio',
                  id: 'layout',
                  label: Drupal.theme('ckEditorLayoutsSelectLabel', Drupal.t('Please choose the layout you want')),
                  items: layouts,
                  style: 'display: block;',
                  default: default_layout_id
                }
              ]
            }
          ],
          // Callback for OK button.
          onOk: function () {
            // Find the selected layout.
            const layout = editor.config.ckeditorLayouts_layouts.find(function (layout) {
              return layout.id === this.getValueOf('layouts', 'layout');
            }, this);

            // Insert the template of the selected layout in the editor.
            editor.insertHtml(layout.template || "");
          }
        };
      });

      // Add a command to open our dialog.
      editor.addCommand('insertCKEditorLayout', {
        allowedContent: 'div[class]',
        exec: function (editor) {
          editor.openDialog('AddLayoutsDialog' + editor.name);
        }
      });

      // Add a button to trigger the command that opens our dialog.
      if (editor.ui.addButton) {
        editor.ui.addButton('AddLayout', {
          label: Drupal.t('Insert a layout'),
          command: 'insertCKEditorLayout',
          icon: this.path + 'icons/layouts.png'
        });
      }
    }
  });

  /**
   * Themes the label for a single layout option.
   *
   * @param label
   *   The label (text) of the layout plugin.
   * @param image
   *   The image associated with the layout plugin.
   *
   * @returns {string}\
   *   The HTML for the layout option label.
   */
  Drupal.theme.ckEditorLayoutsLayoutLabel = function (label, image) {
    return '<br><small>' + label + '</small><br><br>' + image;
  }

  /**
   * Themes the label for the layouts selector.
   *
   * @param label
   *   The label text.
   *
   * @returns {string}
   *   The HTML for the layouts selector label.
   */
  Drupal.theme.ckEditorLayoutsSelectLabel = function (label) {
    return '<p><strong>' + label + ':</strong></p><br />';
  }
})(Drupal, CKEDITOR);
